Assessment for GitLab IT System Engineer Role

Hi!

I wanted to explain the methodology I used for the exercise.

I used Python 3.9. (Please use Python 3.8.3 or higher.)

In working through this exercise, there are two ways I could approach it:
1. Iterate through gl_dept_group obtaining the membership data for each group in stages.yml.
2. Process stages.yml to find the group membership and then validate the group names
   against gl_dept_group, which enforces gl_dept_group as the SSOTi for group names.

I opted for the latter option.

In my verification stage, I noticed that some group names use a dash (-) in gl_dept_group
(e.g. eng-dev-mobile-moble-devops), whereas an underscore (_) is used in stages.yml
(ex. eng-dev-mobile-moble_devops). The first approach would not programatically find
this inconsistency. Instead, the group specified by gl_dept_group would just return no
members, while in reality, stages.yml does have membership information; it's just that
the underscore will prevent the membership from being obtained.

I know from managing our group's documentation library that living documents will be
susceptible to conflicts and inconsistencies, so in this instance, I believe I would submit an MR
to start the discussion to have the corrections made within stages.yml.

For this code, in particular, I output two files:

groups.yml => Contains groups with names validated against gl_dept_group and their members

verify_reqd.yml => Contains groups and members, but these groups failed verification against
               gl_dept_group, usually because of underscores, capitalization, etc.

Thank you for taking the time to look over my assessment.

Kind regards,
Chad

