import yaml
import os
import sys
from pprint import pprint

# TODO: Could have this as a command line parameter.
section_prefix = "eng-dev"

# These are the array keys that refer to team members.
group_member_array_keys={"pm","pmm","cm","product_sponsor","support","sets","pdm","ux","uxr","tech_writer","tw_backup","appsec_engineer"}

# These are the array keys that refer to managers.
manager_member_array_keys={"backend_engineering_manager","frontend_engineering_manager"}

# Some positions are listed, but not filled (e.g. TBD). These positions should be skipped when creating the groups.
position_placeholder={"TBD"}

# Grab the current working directory.
pwd = os.path.dirname(os.path.abspath(__file__))

# Grab the full path to stages.yml.
stages_yml = os.path.join(pwd, 'stages.yml')

# Open stages.yml.
try:
    with open(stages_yml, 'r') as f:
        stages = yaml.safe_load(f)
except FileNotFoundError:
    print("The file " + stages_yml + " does not exist")
    sys.exit(-1)

# Grab the full path to teams.yml.
team_yml = os.path.join(pwd, 'team.yml')

# Open teams.yml
try:
    with open(team_yml, 'r') as f:
        team = yaml.safe_load(f)
except FileNotFoundError:
    print("The file " + team_yml + " does not exist")
    sys.exit(-1)

# Grab the full path to gl_dept_group
gl_dept_groups = os.path.join(pwd, 'gl_dept_group')

# Open gl_dept_group
try:
    gl_dept_groups_file = open(gl_dept_groups, "r")
except FileNotFoundError:
    print("The file " + gl_dept_groups + " does not exist")
    sys.exit(-1)

# Convert the gl_dept_group to a set so we can quickly see if an element is a member
# via "in."
# https://stackoverflow.com/questions/12330522/how-to-read-a-file-without-newlines
gl_dept_group = set(gl_dept_groups_file.read().splitlines())
gl_dept_groups_file.close()

# Grab the GitLab handle based upon a name from team.yml (team).
def get_team_member_handle(team_member):
    # https://stackoverflow.com/questions/8653516/python-list-of-dictionaries-search
	team_member_handle = list(filter(lambda person: person['name'] == team_member, team))
	if len(team_member_handle) > 0:
	    return team_member_handle[0]['gitlab']
	else:
	    return ''

def get_team_members(current_level):
    # current_level can be a stage or group.

    # Maintain the team members and managers in a list.
    team_members = []
    managers = []

    # Begin transversing the array keys in the current level.
    for key in current_level.keys():
        # If the array key is in the group_member_array_keys set, then it could have a team member.
        if key in group_member_array_keys.union(manager_member_array_keys):
            # Some array keys have lists. If that is the case, we need to transverse the list.
            if type(current_level[key]) == list:
                # Start examining each element in the list...
                for member in current_level[key]:
                    # As long as it's not TBD...
                    if member not in position_placeholder:
                        # If the team member is in one of the two manager array keys, then that team member is a manager.
                        # Also note that we are getting the team member's GitLab handle from team.yml (team).
                        if key in manager_member_array_keys:
                            managers.append({'manager_name':member,'manager_handle':get_team_member_handle(member)})
                        else:
                            team_members.append({'member_name':member,'member_handle':get_team_member_handle(member)})
            else:
                # In this case, a single team member occupies the role.
                #team_members.append({'member_name':current_level[key]})
                if current_level[key] not in position_placeholder:
                    # Get the team member...
                    member = current_level[key]
                    # If the team member is in one of the two manager array keys, then that team member is a manager.
                    # Also note that we are getting the team member's GitLab handle from team.yml (team).
                    if key in manager_member_array_keys:
                        managers.append({'manager_name':member,'manager_handle':get_team_member_handle(member)})
                    else:
                        team_members.append({'member_name':member,'member_handle':get_team_member_handle(member)})
    # Return the managers and team_members lists.
    return managers, team_members

def main():

    # Initialize the output lists.
    group_info_ok = []
    group_info_needs_verification = []

    # We are going to transverse in the following order:
    # 1. Process the stage
    # 2. Process the groups within the stage
    for stage in stages['stages']:
        # Get the members that are at the stage level (e.g. manage)
        group_name = section_prefix + "-" + stage

        # Get the managers and team members for the current stage.
        managers, team_members = get_team_members(stages['stages'][stage])

        if group_name in gl_dept_group :
        # If the group name matches a group name in the gl_dept_group set, then we have
        # a consistent and valid group.
            group_info_ok.append({'group_name':group_name,'group_managers':managers, 'group_members':team_members})
        else:
            # We could not find the group in gl_dept_group, so we need further investigation and perhaps an MR.
            group_info_needs_verification.append({'group_name':group_name,'group_managers':managers, 'group_members':team_members})
        for group in stages['stages'][stage]['groups']:
            # Now process the groups in the stage.
            group_name = section_prefix + "-" + stage + '-' + group

            # Get the managers and team members for the current group.
            managers, team_members = get_team_members(stages['stages'][stage]['groups'][group])
            # If the group name matches a group name in the gl_dept_group set, then we have
            # a consistent and valid group.
            if group_name in gl_dept_group :
                group_info_ok.append({'group_name':group_name, 'group_managers':managers, 'group_members':team_members})
            else:
                group_info_needs_verification.append({'group_name':group_name, 'group_managers':managers, 'group_members':team_members})

    with open('groups.yml', 'wt') as out:
        pprint(group_info_ok, width=100, compact=False, sort_dicts=False, stream=out)
    with open('verify_reqd.yml', 'wt') as out:
        pprint(group_info_needs_verification, width=100, compact=False, sort_dicts=False, stream=out)

if __name__ == "__main__":
    main()
